import React from 'react';

class Banner extends React.Component {
  render() {
    const bannerStyle = {
      background: "url('app/utils/banners/Banner.jpg') center no-repeat",
      backgroundSize: "100% 100%"
    };
    return (
      <div style={bannerStyle} className="banner">
        <div className="offer-text">CHOOSE YOUR CASHBACK OFFER</div>
        <div className="vip">VIP</div>
      </div>
    )
  }
}

export default Banner;
