import React from 'react';
import Banner from './Banner';
import Email from './Email';
import fire from '../src/fire';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = { emails : [] };
  }

  componentWillMount() {
    let emailsRef = fire.database().ref('emails').orderByKey().limitToLast(50);
      emailsRef.on('child_added', snapshot => {
        let email = { text: snapshot.val(), id: snapshot.key };
        this.setState({ emails: [email].concat(this.state.emails) });
    })
  }

  render() {
    return (
      <div>
        <Banner />
        <Email emails={this.state.emails} />
      </div>
    )
  }
}

export default App;
